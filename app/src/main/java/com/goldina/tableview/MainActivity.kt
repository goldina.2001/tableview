package com.goldina.tableview

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var imageView: ImageView
    private lateinit var button: Button
    private lateinit var textView: TextView
    private lateinit var textHeadView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageView = findViewById(R.id.imageView)
        button = findViewById(R.id.button)
        textView = findViewById(R.id.textView)
        textHeadView = findViewById(R.id.textView2)
        button.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val color = Color.argb(255, Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
                button.setBackgroundColor(color)
                textView.setTextColor(color)
                textHeadView.setTextColor(color)
                Random.nextInt(6).let{
                    when(it){
                        0 -> {
                            imageView.setBackgroundResource(R.drawable.circles)
                            textView.text = "Circles. 2020"
                        }
                        1 -> {
                            imageView.setBackgroundResource(R.drawable.swimming)
                            textView.text = "Swimming. 2018"
                        }
                        2 -> {
                            imageView.setBackgroundResource(R.drawable.tdf)
                            textView.text = "The Divine Feminine. 2016"
                        }
                        3 -> {
                            imageView.setBackgroundResource(R.drawable.goodam)
                            textView.text = "GO:OD AM. 2015"
                        }
                        4 -> {
                            imageView.setBackgroundResource(R.drawable.kids)
                            textView.text = "K.I.D.S. 2010"
                        }
                        5 -> {
                            imageView.setBackgroundResource(R.drawable.faces)
                            textView.text = "Faces. 2021"
                        }

                    }
                }

            }

        })
    }
}